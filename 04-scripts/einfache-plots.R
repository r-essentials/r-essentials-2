# Einfache Plots

# Pakete laden --------------------------------------------------------------------------------

library(tidyverse)
library(cowplot)
library(ggthemes)

# Daten laden ---------------------------------------------------------------------------------

iris |> as_tibble()

# Visualisierungen ----------------------------------------------------------------------------

ggplot()

# Es lohnt sich die Hilfeseite anzusehen.
?ggplot

# Data --------------------------------------------------------------------

ggplot(
    data = iris
)

# Mappings (Aesthetics) ---------------------------------------------------

plot_orig <-
    ggplot(
        data = iris,
        mapping = aes(
            x = Sepal.Length,
            y = Sepal.Width
        )
    )

plot_orig

?aes

# Geometries --------------------------------------------------------------

plot_phase1 <-
    plot_orig +
        geom_point()

plot_orig <- plot_phase1

plot_orig

# Geometries und Mappings -------------------------------------------------

ggplot(
    data = iris,
    mapping = aes(
        x = Sepal.Length,
        y = Sepal.Width
    )
) +
    # Reihenfolge der GEOMETRIES wichtig!
    geom_point(
        size = 5,
        color = "red"
    ) +
    geom_point(
        mapping = aes(
            color = Species
        ),
        color = Species,
        # Parameter außerhalb von mapping haben vorrang
        show.legend = FALSE,
        size = 2
    )

Species <- "green"

# Theming -----------------------------------------------------------------

ggplot(
    data = iris,
    mapping = aes(
        x = Sepal.Length,
        y = Sepal.Width
    )
) +
    # Reihenfolge der GEOMETRIES wichtig!
    geom_point(
        size = 4,
        color = "darkred"
    ) +
    geom_point(
        mapping = aes(
            color = Species
        ),
        size = 2,
        show.legend = FALSE
    ) +
    scale_color_manual(values = c("red", "blue", "yellow")) +
    # scale_color_brewer(palette = "Accent") +
    # scale_color_brewer(palette = "Set1")
    theme_dark(base_size = 14, base_family = "Times New Roman") +
    theme(
        axis.ticks.x = element_blank(),
        axis.title.x = element_text(
            # family = "Arial",
            colour = "red",
            face = "bold"
        ),
    )

# Plots als Plot-Objekte verstehen ----------------------------------------

mein_kleiner_plot <-
    ggplot(
        data = iris,
        mapping = aes(
            x = Sepal.Length,
            y = Sepal.Width
        )
    ) +
    # Reihenfolge der GEOMETRIES wichtig!
    geom_point(
        size = 4,
        color = "darkred"
    ) +
    geom_point(
        mapping = aes(
            color = Species
        ),
        size = 2,
        show.legend = FALSE
    ) +
    theme_dark(base_size = 20, base_family = "Times New Roman") +
    theme(
        axis.ticks.x = element_blank()
    )

mein_kleiner_plot

## Theme Beispiele

mein_kleiner_plot +
    theme_minimal_vgrid()

## APA Beispiel (Theming)

plot_final <-
    mein_kleiner_plot +
        geom_boxplot(
            mapping = aes(
                color = Species
            )
        ) +
        theme_bw() +
        theme(
            panel.grid.major=element_blank(),
              panel.grid.minor=element_blank(),
              panel.border=element_blank(),
              axis.line=element_line(),
              axis.text= element_text(face = "bold"),
              text=element_text(family='Times'),
              plot.title = element_text(family="Times", face="bold", size=16, hjust = 0)
            ) +
        ggtitle("Mein erster Plot", "Und nun?")

mein_kleiner_plot
plot_final

# Speichern und Laden eines Plot-Objekts ----------------------------------

write_rds(
    x = plot_final,
    file = "repository/plot-final-gespeichert.rds",
    compress = "gz"
)


plot_den_ich_bekommen_habe <- read_rds(file = "repository/plot-final-gespeichert.rds")

plot_den_ich_bekommen_habe_2 <- read_rds(file = "repository/plot-final-gespeichert.rds")

plot_den_ich_bekommen_habe
