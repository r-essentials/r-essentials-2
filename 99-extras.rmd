---
title: "Extras"
subtitle: "R::Essentials_2"
author: "Fabian Mundt"
date: 02.04.2020 -- 03.04.2020
output: 
    tufte::tufte_html:
        highlight: pygments
        tufte_variant: envisioned
editor_options: 
  chunk_output_type: console
---

Hier finden Sie einige Links rund um R. Sie sind eingeladen die Liste zu ergänzen.

## Cheatsheets

- https://www.rstudio.com/resources/cheatsheets/

## Literatur

- https://socviz.co
- https://clauswilke.com/dataviz/
- https://r4ds.had.co.nz
- https://adv-r.hadley.nz
- https://bookdown.org/yihui/rmarkdown/
- https://bookdown.org/yihui/rmarkdown-cookbook/
- https://www.edwardtufte.com

## R und Versionsverwaltung (git)

- https://support.rstudio.com/hc/en-us/articles/200532077-Version-Control-with-Git-and-SVN
- https://stackoverflow.com/questions/2712421/r-and-version-control-for-the-solo-data-analyst

## R Markdown für wissenschaftliche Projekte

- https://bookdown.org/home/
- https://github.com/rstudio/pagedown
- https://rstudio.github.io/distill/

## R Markdown und das Web

- https://bookdown.org/yihui/blogdown/
- http://shiny.rstudio.com/articles/interactive-docs.html
- http://shiny.rstudio.com/articles/dashboards.html

## Nerd Fonts 

- https://github.com/tonsky/FiraCode
- https://nerdfonts.com

## GDA Pakete

- http://factominer.free.fr
- https://rpkgs.datanovia.com/factoextra/index.html
- https://github.com/inventionate/TimeSpaceAnalysis

## Masterclass: Vim

- https://neovim.io
- https://github.com/jalvesaq/Nvim-R
- https://github.com/vim-pandoc/vim-pandoc
- https://github.com/vim-pandoc/vim-pandoc-syntax
- https://draculatheme.com/vim/
